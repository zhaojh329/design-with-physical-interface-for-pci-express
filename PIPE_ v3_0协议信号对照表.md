#! https://zhuanlan.zhihu.com/p/454937286
## PIPE 协议

PCIe物理层接口（**Physical Interface for PCI Express**，PIPE）定义了**物理层**中的，**媒介层**（Media  Access Layer，MAC）**和物理编码子层**（Physical Coding  Sub-layer，PCS）**之间的统一接口**，PIPE 协议有助于使不同厂家之间的设备有更好的兼容性。

MAC和PCS都属于PCIe中的**物理层逻辑子层部分**，而PMA（Physical  Media Attachment  Layer）则属于物理层电气子层，如下图所示：



其中**PIPE规范是由Intel提出的行业建议**，并非PCI-SIG规定的PCIe标准之一，PCIe设备厂家完全可以自主选择是否采用PIPE规范。

虽然PIPE规范最早是用于PCIe总线中的（从命名方式就可以看出来），但是该规范的后续版本逐渐开始支持了其他的串行接口。以Intel发布的最新版本的PIPE  Spec为例（v5.1），该版本涉及PCI Express、SATA、USB、DisplayPort和Converged   IO等多种高速串行接口。

## 信号速览表

PIPE 信号速览表旨在为 PCIe 设计验证人员提供一个快速归类、连接和检查的实用工具。

速览表包括信号的各项属性：

- 信号名字
- 信号位宽，/ 表示兼容多种情况
- 信号方向，输入输出方向是相对 PHY 来说的
- 信号有效值
- 信号类型，PIPE v3 协议将信号分为 DATA/Command/Status 三类
- 简单信号描述，详细描述请参阅协议文本
- 总线限制：部分信号只在某几种总线类型下需要。
  - ONLY 表示只有后跟的协议需要
  - NOT   表示只有后跟的协议不需要
- 是否可选
  - YES 表示该信号不是必须的

本表有一些备注：

- 信号列表中未包括 PCLK，RESET#，TXRX 等时钟、复位以及串行数据信号。

本期将从比较简单的 v3.0 协议开始，后续将继续制作更新的协议：

- v4.2 ，支持 PCIe 3/USB 3.1
- v4.3 ，支持 PCIe L1SS/PCIe 4/USB 3.1
- v5.1， 支持 PCIe 5

并将在表格中新增一栏，描述相较于先前协议的改变。

## PIPE v3.0  

PIPE v3.0 支持 PCIe2/USB3.0,出版于 2009 年。



## PIPE v3.0  信号列表

| PIPE  v3.0             |           |               |            |          | Support  PCIe2/USB3                                          |                    |              |
| ---------------------- | --------- | ------------- | ---------- | -------- | ------------------------------------------------------------ | ------------------ | ------------ |
| **Name**               | **Width** | **Direction** | **Active** | **Type** | **Description**                                              | **Bus limitation** | **Optional** |
| TxData                 | 32/16/8   | IN            | N/A        | DATA     | Data input bus. Bits [7:0] are the first symbol to  be transmitted |                    |              |
| TxDataK                | 4/2/1     | IN            | N/A        | DATA     | Data/Control for the symbols of transmit  data.     Bit 0 corresponds to the low-byte of TxData |                    |              |
| RxData                 | 32/16/8   | OUT           | N/A        | DATA     | Data output bus. Bits [7:0] are the first symbol to  be transmitted |                    |              |
| RxDataK                | 4/2/1     | OUT           | N/A        | DATA     | Data/Control for the symbols of read data.     Bit 0 corresponds to the low-byte of RxData |                    |              |
| PHY Mode               | 2         | IN            | N/A        | CMD      | Selects PHY operating mode                                   |                    | YES          |
| Elasticity Buffer Mode | 1         | IN            | N/A        | CMD      | Selects Elasticity Buffer operating mode                     | ONLY USB3          | YES          |
| TxDetectRx/Loopback    | 1         | IN            | HIGH       | CMD      | Used to tell the PHY to begin a receiver detection  operation or to begin loopback or to signal LFPS during P0 for USB Polling  state |                    |              |
| TxElecIdle             | 1         | IN            | HIGH       | CMD      | See Section 6.20 (PCI Express Mode) or Section 6.21  (USB SuperSpeed mode) |                    |              |
| TxCompliance           | 1         | IN            | HIGH       | CMD      | Sets  the running disparity to negative. Used when transmitting the PCI Express  compliance pattern. | ONLY PCIE          | YES          |
| TxOnesZeros            | 1         | IN            | HIGH       | CMD      | Causes the transmitter to transmit an alternating sequence of 50-250  ones and 50-250 zeros when transmitting USB SuperSpeed compliance patterns  CP7 or CP8. | ONLY USB3          | YES          |
| RxPolarity             | 1         | IN            | HIGH       | CMD      | Tells  PHY to do a polarity inversion on the received data.  |                    |              |
| RxEqTraining           | 1         | IN            | HIGH       | CMD      | Used to instruct the receiver to bypass normal operation to perform  equalization training. | ONLY USB3          | YES          |
| PowerDown              | 2         | IN            | N/A        | CMD      | Power up or down the transceiver.                            |                    |              |
| Rate                   | 1         | IN            | N/A        | CMD      | Control the link signaling rate                              |                    | YES          |
| TxDeemph               | 2         | IN            | N/A        | CMD      | Selects transmitter de-emphasis                              |                    | YES          |
| TxMargin               | 3         | IN            | N/A        | CMD      | Selects transmitter voltage levels.                          | NOT PCIE GEN1      | YES          |
| TxSwing                | 1         | IN            | N/A        | CMD      | Controls transmitter voltage swing level                     |                    | YES          |
| RX Termination         | 1         | IN            | HIGH       | CMD      | Controls presence of receiver terminations:                  | ONLY USB3          | YES          |
| RxValid                | 1         | OUT           | HIGH       | STATUS   | Indicates symbol lock and valid  data on RxData and RxDataK. |                    |              |
| PhyStatus              | 1         | OUT           | HIGH       | STATUS   | Used to communicate completion  of several PHY functions including stable PCLK after Reset# deassertion,  power management state transitions, rate change, and receiver detection. |                    |              |
| RxElecIdle             | 1         | OUT           | HIGH       | STATUS   | Indicates receiver detection of an electrical idle.  While deasserted with the PHY in P2 (PCI Express mode) or the PHY in P0, P1,  P2, or P3 (USB SuperSpeed mode), indicates detection of either:PCI Express  Mode: a beacon.     USB SuperSpeed Mode : LFPS |                    |              |
| RxStatus               | 3         | OUT           | N/A        | STATUS   | Encodes receiver status and error codes for the  received data stream when receiving data |                    |              |
| PowerPresent           | 1         | OUT           | HIGH       | STATUS   | USB SuperSpeed Mode: Indicates the presence of  VBUS.        | ONLY USB3          | YES          |



## 协议下载

https://www.intel.com/content/dam/doc/white-paper/usb3-phy-interface-pci-express-paper.pdf



## 参考文献

https://zhuanlan.zhihu.com/p/444919188





