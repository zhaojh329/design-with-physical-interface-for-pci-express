# PHY Interface  协议翻译： 3 PHY/MAC interface

PHY Interface For the PCI Express, SATA, and USB 3.1 Architectures 

Version 4.3

©2007 - 2014 Intel Corporation—All rights reserved.

![image-20220507211750572](img/PIPE_2_intoduction/image-20220507211750572.png)

##  *3 PHY/MAC interface*

图 3-1 展示了 PHY 和 MAC 层模块之间的**数据**以及逻辑**命令**/**状态**信号。本文档的第 5 节将会讨论这些信号。支持 PCI Express, SATA, 以及 USB 等总线模式的 PIPE 信号有一些区别，具体哪些模式会用到哪些信号，可以参阅文档的第 6 节详细列表。

![image-20220507212912038](img/PIPE_2_intoduction/image-20220507212912038.png)

图 3-1 PHY/MAC 之间的接口

PIPE 协议支持多种不同的 PHY/MAC 间接口配置，以支持不同的信号速率。

#### 3.1 PCI Express 与 USB 配置

对于 2.5 GT/s 速率的 PCIe 模式，实现可以将数据通道位宽设置为 16 bit，PCLK 时钟频率设置为 125 MHz，或者将数据通道位宽设置为 8 bit，PCLK 时钟频率设置为 250 MHz。

对于支持 2.5 GT/s 以及 5.0 GT/s 两种速率的 PCIe 模式，运行过程中速率可以在两者之间切换，实现中可以选择多种不同的数据位宽和时钟频率组合。

首先，可以将时钟速率固定为 250MHz, 速率为 2.5 GT/s  时使用 8 bit 数据位宽，而在 5.0 GT/s 速率下使用 16 bit 数据位宽。

另一种实现方案是固定数据位宽，根据不同的数据速率来调整 PCLK 的频率。在这种情况下，数据通路位宽固定为 8 bit 的实现中， 速率为 2.5 GT/s  时 PCLK 工作在 250MHz，而在 5.0 GT/s 速率下 PCLK 频率提升为 500MHz。

类似的，如果数据通路固定为 16 bit，速率为 2.5 GT/s  时 PCLK 工作在 125MHz，而在 5.0 GT/s 速率下 PCLK 频率提升为 250MHz。

每种速率模式下 PCLK 频率与数据位宽的组合，总结为表 3-2 所示

表 3-2 PCIe 各速率模式下，可选的时钟频率与数据位宽组合

![image-20220507220647784](img/PIPE_2_intoduction/image-20220507220647784.png)

![image-20220507220828936](img/PIPE_2_intoduction/image-20220507220828936.png)

一组兼容 PIPE 协议的 MAC 和 PHY 之间的接口，仅需为每种 PCIe 速率支持一种实现方案即可。

#### 3.2  USB 配置

同样的，对于支持 5 GT/s 以及 10 GT/s 速率 USB 总线的 PIPE 实现，可以从表 3-3 选择一组时钟频率和数据位宽的方案。

表 3-3 USB 各速率模式下，可选的时钟频率与数据位宽组合

![image-20220507222108492](img/PIPE_2_intoduction/image-20220507222108492.png)

一组兼容 PIPE 协议的 MAC 和 PHY 之间的接口，仅需为每种 USB 速率支持一种实现方案即可。

#### 3.3 SATA 配置

For SATA PIPE implementations that support only the 1.5 GT/s signaling rate implementers can  choose to have 16 bit data paths with PCLK running at 75 MHz, or 8 bit data paths with PCLK  running at 150, 300 or 600 MHz.

The 300 and 600 Mhz options requires the use of TxDataValid  and RxDataValid signals to toggle the use of data on the data bus. 

SATA PIPE implementations that support 1.5 GT/s signaling and 3.0 GT/s signaling in SATA  mode, and therefore are able to switch between 1.5 GT/s and 3.0 GT/s signaling rates, can be  implemented in several ways. 

An implementation may choose to have PCLK fixed at 150 MHz  and use 8-bit data paths when operating at 1.5 GT/s signaling rate, and 16-bit data paths when  operating at 3.0 GT/s signaling rate. 

Another implementation choice is to use a fixed data path  width and change PCLK frequency to adjust the signaling rate. In this case, an implementation  with 8-bit data paths could provide PCLK at 150 MHz for 1.5 GT/s signaling and provide PCLK  at 300 MHz for 3.0 GT/s signaling. 

Similarly, an implementation with 16-bit data paths would  provide PCLK at 75 MHz for 1.5 GT/s signaling and 150 MHz for 3.0 GT/s signaling. The full  set of possible widths and PCLK rates for SATA mode are shown in Table 3-1 SATA Mode – Possible PCLK rates and data widths . 

![image-20220507223136871](img/PIPE_2_intoduction/image-20220507223136871.png)

![image-20220507223151195](img/PIPE_2_intoduction/image-20220507223151195.png)

一组兼容 PIPE 协议的 MAC 和 PHY 之间的接口，仅需为每种 SATA 速率支持一种实现方案即可。

#### 3.4 其他规则

注意：当 MAC 实现了 **TxDataValid** 信号，而当前模式无需 **TxDataValid** 信号来指示有效数据时，MAC 需要始终保持 **TxDataValid** 高电平有效。

同样地，当 PHY 实现了 **RxDataValid** 信号，而当前模式无需 **RxDataValid** 信号来指示有效数据时，MAC 需要始终保持 **RxDataValid** 高电平有效。

可能有一些 PIPE 实现方案会支持上述表格中提到的多项配置方案。以下是关于支持多方案实现的一些规则。

如果 PHY 支持同一速率下的多种数据位宽和时钟频率配置方案，那么 PHY 也必须实现数据位宽（Width）和 PCLK 频率（rate）两项 PIPE 接口控制信号，用于位宽和速率的选择。

如果 PHY 支持 PCI Express 模式或者 SATA  模式或者 USB 模式中的多种数据速率，那么 PHY 需要支持所有其所支持的速率下，基于该固定 PCLK 频率的配置，或者所有速率下基于该固定数据位宽的配置。

> 译注：比如 PHY 支持 PCIe 2.5/5/8 GT/s 速率，如果其实现了 250 MHz@32bit（8GT/s），那么 PHY 也需要支持  250 MHz@16bit（5GT/s）以及 250 MHz@8bit（2.5GT/s），即所有的 250MHz 方案。



------


译者： LJGibbs

校对:	

欢迎浏览 《Design with Physical Interface for PCI Express 》 的 Gitee 仓库，里面是笔者收集翻译的相关文档。

[Design with Physical Interface for PCI Express](https://gitee.com/ljgibbs/design-with-physical-interface-for-pci-express)



另外为 《Mindshare PCI Express Technology 3.0 一书的中文翻译计划》打个广告，欢迎参与翻译/校对

[Mindshare PCI Express Technology 3.0 一书的中文翻译计划](https://gitee.com/ljgibbs/chinese-translation-of-pci-express-technology)
