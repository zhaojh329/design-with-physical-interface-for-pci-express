# PHY Interface  协议翻译： 7 PIPE Operational Behavior

PHY Interface For the PCI Express, SATA, and USB 3.1 Architectures 

Version 4.3

©2007 - 2014 Intel Corporation—All rights reserved.

![image-20220507211750572](img/PIPE_2_intoduction/image-20220507211750572.png)

##  *7 PIPE Operational Behavior*

#### *7.1 Clocking*

PHY 接口组件中会使用到 3 个时钟信号。第一个时钟信号，CLK，是 PHY 的参考时钟，用于产生内部数据发送接收比特流时钟。协议中没有该时钟的具体定义，定义基于实现时的细节，必须由厂商在实现时定义。对于不同工作模式的 PHY，协议定义会有所不同。CLK 可能拥有扩频调制功能，因为其需满足系统的参考时钟需求。（举例而言，Card Electro Mechanical Specification 可能需要参考时钟具有扩频调制功能）

第二个时钟信号，PCLK，在 “PCLK as PHY Output” 模式下是 PHY 输出给外部逻辑的时钟，在 “PCLK as PHY Input ” 模式下是每个数据通道 PHY 统一使用的外部时钟输入，并行接口使用统一的外部输入时钟来同步各个数据通道的数据。PCLK 的频率取决于数据速率、PCLK 频率选择、以及控制输入以及输出数据位宽的 PHY 工作模式。PCLK 上升沿是数据采样的参考点。PCLK 也可能支持扩频调制。

第三个时钟信号，MAX PCLK，是一个输出频率恒定的时钟信号，频率由 PHY 所支持的最高传输速率决定。 PHY 仅在  “PCLK as PHY Input ” 模式，或者是在所有支持 PCIe 3.0 的模式中需要 MAX PCLK。

#### *7.2 Reset*

当 MAC 想要复位 PHY 时（比如在上电初始化时），MAC 必须将 PHY 置于复位状态，直到电源和时钟供应稳定。

在 MAC 移除复位信号后，PHY 在输入时钟 PCLK 以及/或者 MAX PCLK 稳定（比如 PCLK 以及/或者 MAX PCLK 到达正常工作频率至少一个时钟周期），并且供电稳定时，通过置低 PhyStatus 信号通知 MAC 其时钟和电源已经达到稳定。

在 PHY 的复位信号 RESET# 有效期间，MAC 发送给 PHY 的 PIPE 信号必须保持以下要求：

- TxDetectRx/Loopback 置低
- TxElecIdle 置高
- TxCompliance 置低
- RxPolarity 置低
- PowerDown = P1 (PCI Express 模式) 
  - PowerDown = P2 (USB 模式) 
  - PowerDown set to the default value reported by the PHY (SATA 模式)
- TxMargin = 000b
- TxDeemp = 1
- PHY Mode 设置为 PHY 的实际工作模式
- Rate 设置为 2.5GT/s (PCI Express 模式) 
  - 5.0 GT/s or 10 GT/s (PHY 最高支持的速率)  (USB 模式) 
  - any rate supported by the PHY (SATA 模式)
- TxSwing 复位默认值基于具体实现
- RxTermination 置高  (USB 3.0 模式) 

![image-20220507234936307](img/PIPE_2_intoduction/image-20220507234936307.png)

图 7- 0 PHY 复位释放时序图，关于 PCLK，RESET#以及 PhyStatus

#### *7.3 Power Management – PCI Express Mode* 

电源管理相关的信号用于指导 PHY 最小化电源的消耗。PHY 必须满足各个电源状态下，*PCI Express Base Specification* 中关于时钟恢复和链路训练的时序约束。PHY 还需要满足所有的发送和接收机关于终结（termination）的需求。

PCIe 定义了四种电源状态，分别是 P0、P0s、P1 以及 P2。P0 是 PHY 的正常工作状态。当 PHY 从 P0 进入其他低功耗状态时，PHY 最好可以立刻实现各个状态中规定的低功耗措施。PHY 还需要最多实现四种额外的 PHY 专用电源状态。MAC 在满足 *PCI Express Base Specification* 规定的所有需求的前提下，使用任意的 PHY 专用电源状态。

在 P0、P0s、P1 状态中，PCLK 需要保持运行状态。在上述三种状态之间，以及和 PCLK 保持运行状态的 PHY 专用状态之间，进行状态切换时，PHY 通过置起 PhyStatus 一个周期来表示一次成功的状态切换，切换进入预期的状态。

而对于进出 P2 状态，或者其他 PCLK 不再保持运行状态的 PHY 专用状态时，MAC 必须等到 PHY 表示状态已经切换完成后，再进行需要 PCLK 的操作（Operational sequence）或者后续的电源状态切换。

下面讨论了如何将 PHY 电源状态（译注：Px）映射到协议规定的 LTSSM 链路状态中（译注：Lx）。只要 MAC 满足 PCI *Express Base Specification* 规定的所有需求的前提下，MAC 也可以选择使用 PHY 专用电源状态。

- P0 状态： 所有的内部时钟都在工作。P0 是 PHY 发送接收 PCIe 信号的唯一状态。P0 对应 LTSSM 中大部分的状态，除了下述提到的这些低功耗 PHY 状态以外。

- P0s 状态：PHY PCLK 输出时钟必须保持工作。MAC 只能在发送通道空闲时，将 PHY 转移进入该状态。P0s 状态对应 LTSSM 中的发送机 *Tx_L0s.Idle* 状态。

  当 PHY 处于 P0 或者 P0s 状态时，如果接收机检测到接收方向上的电气空闲，PHY 的接收机部分逻辑可以采取对应的省电措施。但注意到，当接收通道上接收信号恢复后，PHY 必须能够在 PHY 规定的时间（N_FTS with/without common clock）内重新完成比特以及符号同步锁定。当然，这项规定只对 PHY 曾经在先前的 P0 或者 P0s 状态，完成过符号或者比特同步锁定的情况而言。

- P1 状态：PHY 部分选定的内部时钟可以被关闭，但是 PCLK 必须保持工作。MAC 只能在发送和接收通道都空闲时，才能将 PHY 转移进入该状态。PHY 在 PCLK 稳定以及工作电压（DC common mode voltage）稳定并电压值满足协议要求之后，才能宣称进入了 P1 状态（通过置高 PhyStatus 信号）。P1 对应于 LTSSM 中的 *Disabled* 状态，所有 *Detect* 状态以及 *L1.idle* 状态。

- P2 状态： PHY 部分选定的内部时钟可以被关闭，并行总线均处于异步模式，PCLK 也被关闭。

PCLK as PHY Output: 该模式下，当 PHY 状态转移为 P2 时，PHY 必须在 PCLK 关闭前置高 PhyStatus，在 PCLK 完全关闭，PHY 进入 P2 状态后，置低 PhyStatus。在 PHY 从 P2 状态退出时，PHY 需要尽可能快地置高 PhyStatus，并且保持置高状态，直至 PCLK 稳定。

PCLK as PHY Input: 该模式下，当 PHY 状态转移为 P2 时，PHY 需要置高 PhyStatus 一个输入时钟周期，表示其准备好关闭 PCLK。在 PHY 从 P2 状态退出时，PHY 需要尽可能快地置高 PhyStatus 一个输入时钟周期，表示进入 P0 状态，并为重新工作做好准备。

当 PHY 从一种 PCLK 停止工作的状态，转移到另一种 PCLK 同样不工作的状态时，PHY 需要在状态转移完成后置高 PhyStatus，并保持置高直到 MAC 置高 AsyncPowerChangeAck 信号，之后 PHY 置低 PhyStatus。

PHY 在设计实现中应该尽可能降低 P2 状态中的功耗，因为 PHY 此时需要工作在协议规定的 Vaux 电压范围内（参阅 *PCI Express Base Specification*）

P2 状态对应于 LTSSM 的 *L2.Idle* 状态以及 *L2.TransmitWake* 状态。

![image-20220508112812870](img/PIPE_7_PIPE_operational_behavior/image-20220508112812870.png)

图 7-1 PCLK as PHY Output 模式下，PCIe P2 状态的进入与退出时序图

![image-20220508124028140](img/PIPE_7_PIPE_operational_behavior/image-20220508124028140.png)

图 7-2 PCLK as PHY Input 模式下，PCIe P2 状态的进入与退出时序图

MAC 控制下的 PHY 状态转移行为是有限制的，基于 PCIe 协议中规定的 LTSSM 状态转移图，以及前一段中叙述的 PHY 状态与 LTSSM 的状态关系映射，可以得到合法的状态转移有那么几种：

- P0  --> P0s
- P0 --> P1
- P0 --> P2
- P0s --> P0
- P1 --> P0
- P2 --> P0

这些状态之间的转移条件，参阅 PCIe 协议。

从上述状态转移到 PHY 专用电源状态，或者从专用电源状态转移到上述状态都是 PIPE 协议允许的（另有规定除外），同时 MAC 也需要保证上述 PHY 专用状态转换满足 PCIe 协议的时序要求。

译者供表：PIPE Px 状态与 PCIe 链路状态之间的关系

| PHY 状态 | LTSSM 状态                                    |
| -------- | --------------------------------------------- |
| P0       | 下述状态以外的正常工作状态                    |
| P0s      | *Tx_L0s.Idle*                                 |
| P1       | *Disabled* ，所有 *Detect* 状态以及 *L1.idle* |
| P2       | *L2.Idle* ， *L2.TransmitWake*                |

![image-20220508142341190](img/PIPE_7_PIPE_operational_behavior/image-20220508142341190.png)

译者供图：图来自于 Mindshare PCIe 3.0 文档 14-6，LTSSM 状态转移图 

#### *7.4 Power Management – USB Mode*

翻译暂略

#### *7.5 Power Management – SATA Mode*

翻译暂略

#### *7.6 Changing Signaling Rate, PCLK Rate, or Data Bus Width*

##### *7.6.1 PCI Express Mode*

PCIe 速率、PCLK 频率以及数据总线位宽三项配置的调整，仅能够在 PHY 处于 P0 或者 P1 电源状态，以及 TxElecIdle 和 RxStandby 信号置起的情况下进行。上述三项参数中的任意两项的组合可以被同时调整，但是 MAC 不允许只调整其中仅仅一项。（译注：这种情况实际上是做不到的，另外从协议来看，三项参数同时调整也是不可行的）

在 PCLK as PHY Output 模式下，当 MAC 调整三项配置（PCIe 速率、PCLK 频率以及数据总线位宽）中的两项时，PHY 在完成配置更新后，置起一个周期的 PhyStatus 表示更新完成。MAC 在 PHY 通过 PhyStatus 表示本次配置更新完成前，不能够进行后续的操作，包括电源状态转换，置低 TxElecIdle 和 RxStandby 信号，或者开始下一次配置更新。

在 PCLK as PHY Input 模式下，配置更新的过程和前一模式相同，唯一的区别在于，MAC 需要知道何时输入 PCLK 时钟频率可以被安全地调整。MAC 只能够在 PHY 置高 PclkChangeOk 信号后，才能调整 PCLK_Rate 信号和 PCLK 时钟信号频率。

MAC 调整 PCLK 频率后，置起 PclkChangeAck 信号与 PHY 进行握手。PHY 通过置起 PhyStatus  信号一个周期表示收到 MAC 发出的握手信号，随后在 PhyStatus 下降沿置低 PclkChangeOk 信号。注意，PHY 仅在 MAC 调整 PCLK_Rate 时，使用 PclkChangeOk 信号。

MAC 在采样到 PHY 发出的 PclkChangeOk  信号后，置低 PclkChangeAck  信号完成握手。在采样到 PHY 发出的 PhyStatus 高电平后，MAC 也有可能置低 TxElecIdle 和 RxStandby 信号。

随着 LTSSM 状态变化，在几种情况中，PHY 的 PCIe 速率、PCLK 频率、数据总线位宽以及电源状态可能都需要发生变化。一种情况是，LTSSM 转移至 Detect 状态，PHY 在将电源状态从 P0 切换到 P1 之前，（如果需要的话）MAC 必须将速率切换至 2.5  GT/s ，因此需要改变 PCLK 频率或者数据位宽。

另一种情况是，LTSSM 转移至 L2.Idle 状态，PHY 在将电源状态从 P0 切换到 P2 之前，（如果需要的话）同样需要将速率调整到 2.5  GT/s，此时 PHY 的 PCIe 速率、PCLK 频率、数据总线位宽以及电源状态可能都需要发生变化。

一些 PHY 架构可能允许同时改变链路速率以及 PHY 电源状态，涉及到 PCIe 速率、PCLK 频率以及数据总线位宽三项配置的调整。如果 PHY 支持这项特性，那么 MAC 必须在同一个 PCLK 边沿同时调整 PowerDown 信号和 PCLK 频率或者数据总线位宽信号。这种情况可能在 PHY 从 P0 到 P1 或者 P2 的状态改变中发生，电源状态的机制和 7.3 节的描述一致，但此时的完整信号不仅表示电源状态切换完成，也表示PCIe 速率、PCLK 频率或者数据总线位宽调整完成。



##### *7.6.2 USB Mode*

翻译暂略

##### *7.6.3 SATA Mode*

翻译暂略

##### *7.6.4 Fixed data path implementations*

下图展示了  PCLK is a PHY Output 模式下，MAC 通过改变 PCLK ，从而改变速率的信号时序图。

实现通过 PCLK 频率改变，以改变链路速率时，必须在时钟停止后改变 PCLK 频率（如果 PCLK 支持停止的话），以防止使用 PCLK 的计时器溢出。

（译注：原文有些奇怪：Implementations that change  the PCLK frequency when changing signaling rates must change the clock such that the time the  clock is stopped (if it is stopped) is minimized to prevent any timers using PCLK from exceeding  their specifications.）

并且，在 PCLK 频率切换期间，需要保证 PCLK 频率不超过 PHY 支持的最高 PCLK 频率。

从 Rate 信号置起，到 PCLK 频率变化完成的时间间隔，取决于 PHY 的设计实现。这项时序参数会用于置低 TxDataValid 和 RxDataValid 信号，用于在 PCLK 切换期间，停止数据通路传输。

![image-20220508160458172](img/PIPE_7_PIPE_operational_behavior/image-20220508160458172.png)

图  7-3a PCLK is a PHY Output 模式下，MAC 通过改变 PCLK ，从而改变速率的信号时序图。（译注：原文图注为 PHY Input mode，但译者觉得这应该是 PHY Output mode 的时序图）



![image-20220508160356662](img/PIPE_7_PIPE_operational_behavior/image-20220508160356662.png)

图  7-3b PCLK is a PHY Input 模式下，PCIe 速率从 2.5 Gt/s 变换到 5 Gt/s 信号时序图。

##### *7.6.5 Fixed PCLK implementations*

下图展示了 MAC 通过改变有效数据位宽 ，从而改变速率的信号时序图。PCLK 可以在速率变换期间停止。下图中的时序关系，同样可以应用于采用 TxDataValid 和 RxDataValid 信号指示位宽变化的 PCLK 固定速率变换方案。

![image-20220508160604748](img/PIPE_7_PIPE_operational_behavior/image-20220508160604748.png)

图  7-4 MAC 固定 PCLK 频率，通过改变数据位宽，从而改变速率的信号时序图。

##### *7.7 Transmitter Margining – PCI Express Mode and USB Mode*

翻译暂略

##### *7.8 Selectable De-emphasis – PCI Express Mode*

翻译暂略

##### *7.9 Receiver Detection – PCI Express Mode and USB Mode*

在 PCIe 的 P1 状态和 USB 的 P2、P3 状态中，PHY 能够被 MAC 指示开始接收侦测行为，来探测在链路对端是否有设备。

接收检测中的基本流程是 MAC 通过置高  TxDetectRx/Loopback 信号，要求 PHY 开始接收检测。在 PHY 完成一次接收检测后，置高 PhyStatus 一个周期，并将 RxStatus 信号的值更新为接收检测的结果。

在一次接收检测结束后，PHY 置高 PhyStatus 一个周期，MAC 需要首先置低 TxDetectRx/Loopback，才能开始下一次接收检测，或者其他操作，比如电源状态或者速率转换。

一旦 MAC 发出接收检测的请求后（通过置高  TxDetectRx/Loopback 信号），MAC 必须保持 TxDetectRx/Loopback 信号为高，直到 PHY 完成本次接收检测，置高 PhyStatus 信号为止。

在 USB 模式 PHY P3 电源状态下，完成接收检测后，PHY 置高 PhyStatus 信号，并把检测结果更新到 RxStatus ，一直保持两个信号，直至 MAC 置低 TxDetectRx/Loopback 信号。（译注： USB 模式支持 P3 模式下的接收检测，P3 模式没有 PCLK，所以 PhyStatus 会一直置高，而不是只是持续一个周期）

![image-20220508162337490](img/PIPE_7_PIPE_operational_behavior/image-20220508162337490.png)

![image-20220508161341172](img/PIPE_7_PIPE_operational_behavior/image-20220508161341172.png)

图 7-9-1 P1，P2 模式下接收检测时序图

##### 7.10 Transmitting a beacon – PCI Express Mode

当 PHY 被置于 P2 电源状态后，且 MAC 希望发送一个 beacon 信号，MAC 需要置低 TxElecidle，此后 PHY 应该持续产生有效的 beacon 信号，直至 TxElecidle 信号被置起。而在 PHY 返回 P0 状态之前，MAC 必须将 TxElecidle 信号置起。



![image-20220508201908739](img/PIPE_7_PIPE_operational_behavior/image-20220508201908739.png)

图 7-10-1 P2 状态下 PHY 发送 Beacon 信号

##### 7.11 Transmitting LFPS – USB Mode

翻译暂略

##### 7.12 Detecting a beacon – PCI Express Mode

PHY 接收机应当时钟检测线路上的电气空闲状态，除了 PHY 被复位期间。PHY 处于 P2 状态时，当 PHY 置低 RxElecIdle 信号时，代表接收机检测到了线路上发送的 beacon 信号。

![image-20220508201926481](img/PIPE_7_PIPE_operational_behavior/image-20220508201926481.png)

图 7-12-1 P2 状态下 PHY 检测到 Beacon 信号，并置低 RxElecIdle 信号

##### 7.13 Detecting Low Frequency Periodic Signaling – USB Mode

##### 7.14 Clock Tolerance Compensation

##### 7.15 Error Detection

##### 7.16 Loopback

##### 7.17 Polarity Inversion – PCI Express and USBModes

##### 7.18 Setting negative disparity (PCI Express Mode)

##### 7.19 Electrical Idle – PCI Express Mode

##### *7.20 Link Equalization Evaluation*

处于 P0 状态时，PHY 能够被 MAC 指示开始评估链路伙伴的当前发送均衡设置。

均衡设置评估中的基本流程是 MAC 通过置高  RxEqEval 信号，要求 PHY 开始均衡设置评估。在 PHY 完成一次均衡设置评估后，置高 PhyStatus 一个周期，并将 LinkEvaluationFeedback 信号的值更新评估反馈结果。

在一次均衡设置评估结束，即 PHY 置高 PhyStatus 一个周期后，MAC 需要首先置低 RxEqEval ，才能开始下一次均衡设置评估。

一旦 MAC 发出均衡设置评估的请求后（通过置高  RxEqEval 信号），MAC 必须保持 RxEqEval 信号为高，直到 PHY 完成本次均衡设置评估，置高 PhyStatus 信号为止，除非 MAC 因为超时或者其他一些错误情况，需要终止此次评估。

如果 MAC 需要提前终止评估，那么 MAC 可以在 PHY 完成评估前，就置低 RxEqEval 信号。在 MAC 提前终止评估之后，PHY 需要立刻响应，MAC 会因为提前终止而忽视此次评估的结果。

注意：有一种意外的情况，MAC 在置低 RxEqEval 信号，提前终止评估时，在同一个时钟边沿，PHY 完成评估并置起 PhyStatus 信号，那么这种情况下，PHY 不需要进一步的操作。



![image-20220508162545244](img/PIPE_7_PIPE_operational_behavior/image-20220508162545244.png)

图 7-5 一次成功的链路均衡评估请求时序图

注意：

- RxEqEval 可以和 PhyStatus 同时，也可以在其后置低，只要满足在下一次均衡开始前置低即可 
- 支持背靠背的 RxEqEval 请求，可以置低 RxEqEval ，并在一个时钟之后置高，开始下一次均衡评估

![image-20220508162614643](img/PIPE_7_PIPE_operational_behavior/image-20220508162614643.png)

图 7-6 一次结果为 invalid requeset 的链路均衡评估请求时序图

注意：

- InvalidRequest 信号在 RxEqEval 信号置低后才会置高
- InvalidRequest 信号需要在下一次均衡的 RxEqEval 置起的时钟周期置低 
- InvalidRequest 信号可以只持续一个周期

##### *7.21 Implementation specific timing and selectable parameter support*

翻译暂略

##### *7.22 Control Signal Decode table – PCI Express Mode*

下述表格总结了 7 项控制信号中的 4 项，在各个电源模式下不同的行为。（译注：表中似乎只有 3 项控制信号）

| PowerDown[1:0] | TxDetectRx/ <br>Loopback | TxElecIdle | Description                                                  |
| -------------- | ------------------------ | ---------- | ------------------------------------------------------------ |
| P0: 00b        | 0                        | 0          | PHY 正在发送数据。<br/>MAC 正在每个时钟周期提供数据字节。    |
|                | 0                        | 1          | PHY 不在发送数据，处于电气空闲状态                           |
|                | 1                        | 0          | PHY 处于回环模式                                             |
|                | 1                        | 1          | 非法值                                                       |
| P0s: 01b       | Don't care               | 0          | 非法值，MAC 在 P0s 中应始终使 PHY 处于电气空闲状态。<br/>如果采用该非法值，PHY 会出现未定义行为 |
|                | Don't care               | 1          | PHY 没有数据要发送，并且处于电气空闲状态。<br/>请注意，任何在 TxElecIdle 置起前，通过 PIPE 传输，但还没有在模拟端发送的信号，会在模拟接口进入空闲之前被发送出去。 |
| P1: 10b        | Don't care               | 0          | 非法值，MAC 在 P1 中应始终使 PHY 处于 电气空闲状态。<br/>如果采用该非法值，PHY 会出现未定义行为 |
|                | 0                        | 1          | PHY 空闲                                                     |
|                | 1                        | 1          | PHY 正在接收检测                                             |
| P2: 11b        | Don't care               | 0          | PHY 正在发送 Beacon                                          |
|                | Don't care               | 1          | PHY 空闲                                                     |

至于另外三项控制信号

Reset# 信号会改写一切 PHY 行为，进入复位状态。

TxCompliance ，RxPolarity 只会在 PHY 处于 P0 模式，正常流量传输时，会被用到，且只会单独使用，所以没有列在表格中。

注意：上述规则只适用于 Lane 没有被关闭（"turned of"）的情况，见第 9 节（Multi-lane PIPE）所述。



##### *7.23 Control Signal Decode table – USB Mode*

##### *7.24 Control Signal Decode table – SATA Mode*

##### *7.25 Required synchronous signal timings*

##### *7.26 128b/130b Encoding and Block Synchronization (PCI Express 8 GT/s)*

对于每个数据块，（通常来说是 128 比特，但有时候 Retimer 会发送一些稍短或者稍长的 SKP 块），以 8 GT/s 的速率从 PIPE 接口传输给 PHY，而 PHY 经过编码后还需要发送两个新增的比特。

因为 PHY 发送的 130 比特与 PIPE 数据传输位宽不匹配，所以 MAC 需要通过 TxDataValid 来周期性地停止数据传输，以留出一个时钟周期给 PHY 发送其为组帧而额外产生的比特。

举例而言，如果 TxData 位宽为 16 比特，PCLK 频率为 500MHz，那么每传输 8 个块数据之后，MAC 需要置低 TxDataValid 信号一个周期，使 PHY 能够传输额外的 16 比特。

PHY 使用缓存空间来暂存发送数据，以克服 128/130b 编码方式导致的速率不匹配问题。PHY 的缓存必须在 PHY 从复位状态释放后清空，并且必须在 PHY 退出电气空闲状态时为空（所以在重新进入空闲状态时，缓存会被冲刷清空）。同样地，在接收方向 PHY 也需要以同样的方式控制 RxDataValid。

TxDataValid 和 RxDataValid  必须周期性地置低，每 N 个周期置低一个时钟周期。速率  8 GT/s 时，位宽 8 比特时，N 为 4；位宽 16 比特时，N 为 8；位宽 32 比特时，N 为 16；

在退出复位状态，或者电气状态后，MAC 必须在第 N 个数据宽传输结束后立刻首次置低 TxDataValid  信号。

图 7-7 描述了 8 比特位宽时 TxDataValid  信号时序图。图 7-8 描述了 16 比特位宽的情况

![image-20220508214138554](img/PIPE_7_PIPE_operational_behavior/image-20220508214138554.png)

The PHY must first de-assert RxDataValid immediately after the end of the Nth received block transmitted across the PIPE interface following reset or exit  from electrical idle. 

Examples of timings for RxDataValid and other Rx related signals for a 16  bit wide interface are shown in Figure 7-9

![image-20220508214308946](img/PIPE_7_PIPE_operational_behavior/image-20220508214308946.png)

There are situations, such as upconfigure, when a MAC must start transmissions on idle lanes  while some other lanes are already active. In any such situation the MAC must wait until the cycle after TxDataValid is de-asserted to allow the PHY to transmit the backlog of data due to  128b/130b to start transmissions on previously idle lanes.

##### *7.27 128b/132b Encoding and Block Synchronization (USB 10 GT/s)*

------


译者： LJGibbs

校对:	

欢迎浏览 《Design with Physical Interface for PCI Express 》 的 Gitee 仓库，里面是笔者收集翻译的相关文档。

[Design with Physical Interface for PCI Express](https://gitee.com/ljgibbs/design-with-physical-interface-for-pci-express)



另外为 《Mindshare PCI Express Technology 3.0 一书的中文翻译计划》打个广告，欢迎参与翻译/校对

[Mindshare PCI Express Technology 3.0 一书的中文翻译计划](https://gitee.com/ljgibbs/chinese-translation-of-pci-express-technology)
