# PHY Interface  协议翻译： 2 Introduction

PHY Interface For the PCI Express, SATA, and USB 3.1 Architectures 

Version 4.3

©2007 - 2014 Intel Corporation—All rights reserved.

![image-20220507211750572](img/PIPE_2_intoduction/image-20220507211750572.png)

##  *2 Introduction*

PIPE 是适用于  PCI Express, SATA, 以及 USB 等总线架构的物理层协议，全称 The **P**HY **I**nterface for the **P**CI Express, SATA, and USB Architectures (以下简称 PIPE) 。

> 译注：从 PIPE 的缩写看来看出，本协议原本仅为 PCIe 开发。

PIPE 协议设计初衷在于使开发兼容 PCIe，USB 以及 SATA 等总线的多功能 PHY 成为可能。这样的 PHY 可以构成单独的芯片，也可以用于实现更大规模的 ASIC 芯片中的 PHY 硬核单元（HardMicro cell）。

本协议文本定义了一系列必须在 PIPE 协议兼容 PHY 中实现的功能，以及一组 PHY 和 媒体访问层（Media Access Layer , MAC）、链路层芯片（译注：或者模块）之间的标准接口。本协议文本的目的**不是**具体定义 PHY 芯片或者硬核宏单元中的具体架构或者设计。

PIPE 协议定义允许有多种不同的用处，协议中多处引用而不是重复了  PCI Express, SATA, 以及 USB 的协议文本中的内容。PIPE 协议与这三者的标准协议文本中的内容冲突之处，以这三者的协议文本为准。

> 译注：三种协议文本：PCI-Express Base Specification, SATA 3.0 specification 以及 USB 3.1 Specification

本协议提供了一些 MAC 如何通过 PIPE 接口实现功能的信息，包括多种 LTSSM 状态管理，链路状态管理以及其他功能等。读者应该把功能的描述看作是基于协议基本需求的，“其中一种实现方式的指导”。MAC 的设计实现本身是非常自由的，只要设计能够满足相应的 PIPE 协议需求。

PIPE 协议设计初衷之一是为了加速 PCIe 终端节点，SATA 以及 USB 设备的开发。协议定义了一份芯片设计和终端设备厂商可以用于其产品开发的接口。外设以及 IP 厂商可以将产品的设计开发和验证，和 PCI Express, SATA, 以及 USB 相关高速模拟电路的种种问题隔离开来，这能够最小化开发周期中的风险并减少开发时间。

PIPE 协议为接口定义了可选的两种时钟方案。第一种方案 PHY 提供随路时钟（PCLK）输出。另一种方案中，各个通道的PHY （each lane of the PHY）的 PCLK 由外部输入。第二种方案在先前的 4.1 版本的协议中加入，（译注：当前为 4.3 版本），外部输入 PCLK 方案能够使控制器电路或者其他逻辑更好地控制  PIPE 接口的时序，以满足芯片实现中的时序需求。PHY 仅需要实现上述两种方案中的一种，这两种方案分别被称作为 “PCLK as  PHY Output” 以及 “PCLK as PHY Input”。

图 2-1 展示 PIPE 协议在 PCIe 协议（PCI Express Base Specification）中所处的位置

![image-20220507204152540](img/PIPE_2_intoduction/image-20220507204152540.png)

图 2-2 展示 PIPE 协议在 USB 协议（USB 3.1 Specification）中所处的位置

![image-20220507201902171](img/PIPE_2_intoduction/image-20220507201902171.png)

### *2.1 The PCI Express PHY Layer* 

PCIe 物理层负责实现底层协议实现以及信号生成，实现的特性包括：数据串行和并行之间的互相转换，8b/10b 编解码，128b/130b 编解码（8 GT/s），模拟缓存电路，弹性缓存以及接收检测电路。这部分模块的重心是将以 PCIe 协议速率传输的数据，转换到芯片后级电路的时钟域上去。PCIe PHY 的一些关键特性总结如下：

- 标准 PHY 接口兼容各类 PCIe 逻辑层 IP ，并为 PCIe PHY 厂商提供了标准接口

- 支持仅 2.5GT/s ，2.5GT/s / 5.0 GT/s, 以及 2.5 GT/s / 5.0 GT/s / 8.0 GT/s 多种速率数据传输

- 实现串行数据到 8/16/32 比特并行数据转换

- 高速组件的单功能模块集成

- 从 PCIe 串行总线数据流中恢复接收时钟与数据

- 在传输和接收数据时暂存数据

- 支持 compliance pattern 发送中的直接校验控制 ( direct disparity control)

- 8b/10b 编解码以及错误检测

- 128b/130b 编解码以及错误检测

- 接收信号存在侦测

- Beacon 的传输与接收 

- 可变的 Tx Margining，Tx De-emphasis 以及信号摇摆率设置

  

### *2.2 USB PHY Layer* 

USB 物理层负责实现底层协议实现以及信号生成，实现的特性包括：数据串行和并行之间的互相转换，8b/10b 编解码，128b/130b 编解码（**10 GT/s**），模拟缓存电路，弹性缓存以及接收检测电路。这部分模块的重心是将以 USB 协议速率传输的数据，转换到芯片后级电路的时钟域上去。USB PHY 的一些关键特性总结如下：

- 标准 PHY 接口兼容各类 USB 逻辑层 IP ，并为 USB PHY 厂商提供了标准接口

- 支持 5GT/s 以及 /或者 10 GT/s 多种速率数据传输

- 实现串行数据到 8/16/32 比特并行数据转换

- 高速组件的单功能模块集成

- 从 USB 串行总线数据流中恢复接收时钟与数据

- 在传输和接收数据时暂存数据

- 支持 compliance pattern 发送中的直接校验控制 ( direct disparity control)

- 8b/10b 编解码以及错误检测

- 128b/130b 编解码以及错误检测

- 接收信号存在侦测

- LFPS （Low Frequency Periodic Signaling）传输功能

- 可变的 Tx Margining 设置

  

### *2.3 SATA PHY Layer* 

SATA 物理层负责实现底层协议实现以及信号生成，实现的特性包括：数据串行和并行之间的互相转换，8b/10b 编解码，模拟缓存电路，以及弹性缓存。这部分模块的重心是将以 SATA 协议速率传输的数据，转换到芯片后级电路的时钟域上去。SATA PHY 的一些关键特性总结如下：

-  标准 PHY 接口兼容各类 SATA 逻辑层 IP ，并为 SATA PHY 厂商提供了标准接口
-  支持仅 1.5GT/s ，1.5GT/s / 3.0 GT/s, 以及 1.5 GT/s / 3.0 GT/s / 6.0 GT/s 多种速率数据传输
-  实现串行数据到 8/16/32 比特并行数据转换
-  高速组件的单功能模块集成
-  从 SATA 总线数据流中恢复接收时钟与数据
-  在传输和接收数据时暂存数据
-  8b/10b 编解码以及错误检测
-  COMINIT 和 COMRESET 的发送和接收



> 译注： 这几类总线的 PHY 特性确实很相似，只是在具体速率和一些特性上有所不同，这使多功能 PHY 成了一种顺理成章的选择



------


译者： LJGibbs

校对:	

欢迎浏览 《Design with Physical Interface for PCI Express 》 的 Gitee 仓库，里面是笔者收集翻译的相关文档。

[Design with Physical Interface for PCI Express](https://gitee.com/ljgibbs/design-with-physical-interface-for-pci-express)



另外为 《Mindshare PCI Express Technology 3.0 一书的中文翻译计划》打个广告，欢迎参与翻译/校对

[Mindshare PCI Express Technology 3.0 一书的中文翻译计划](https://gitee.com/ljgibbs/chinese-translation-of-pci-express-technology)
