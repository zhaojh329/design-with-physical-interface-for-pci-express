# PHY Interface  协议翻译： 4 PCI Express and USB PHY Functionality

PHY Interface For the PCI Express, SATA, and USB 3.1 Architectures 

Version 4.3

©2007 - 2014 Intel Corporation—All rights reserved.

![image-20220507211750572](img/PIPE_2_intoduction/image-20220507211750572.png)

##  *4 PCI Express and USB PHY Functionality*



#### 4.1 PCI Express 与 USB 配置



------


译者： LJGibbs

校对:	

欢迎浏览 《Design with Physical Interface for PCI Express 》 的 Gitee 仓库，里面是笔者收集翻译的相关文档。

[Design with Physical Interface for PCI Express](https://gitee.com/ljgibbs/design-with-physical-interface-for-pci-express)



另外为 《Mindshare PCI Express Technology 3.0 一书的中文翻译计划》打个广告，欢迎参与翻译/校对

[Mindshare PCI Express Technology 3.0 一书的中文翻译计划](https://gitee.com/ljgibbs/chinese-translation-of-pci-express-technology)
