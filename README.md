# Design with Physical Interface for PCI Express 

#### 介绍

PCIe PHY 接口协议——PIPE 的导学与实用文档

#### 文档
- PIPE 介绍
    -[[转载]PCIe扫盲——PCI Express物理层接口（PIPE）](https://zhuanlan.zhihu.com/p/444919188)

- PIPE 协议手册

    -[ver 3.0]()
    -[Ver 3.0 r9](https://www.intel.cn/content/dam/doc/white-paper/phy-interface-pci-express-sata3-specification-v09.pdf) 
    -[ver 4.3](https://gitee.com/ljgibbs/design-with-physical-interface-for-pci-express/blob/master/spec/phy-interface-pci-express-sata-usb31-architectures-ver43.pdf)
    -[ver 5.1](https://gitee.com/ljgibbs/design-with-physical-interface-for-pci-express/blob/master/spec/phy-interface-pci-express-sata-usb31-architectures-ver51.pdf)
    -[ver 6.1](https://www.intel.com/content/dam/www/public/us/en/documents/white-papers/phy-interface-pci-express-sata-usb30-architectures-3-1.pdf)


#### 工具

- [PIPE 信号速览表 xls](https://gitee.com/ljgibbs/design-with-physical-interface-for-pci-express/blob/master/PIPE_signal_list.xlsx)

- [PIPE 信号速览表 文章](https://gitee.com/ljgibbs/design-with-physical-interface-for-pci-express/blob/master/PIPE_%20v3_0%E5%8D%8F%E8%AE%AE%E4%BF%A1%E5%8F%B7%E5%AF%B9%E7%85%A7%E8%A1%A8.md)
    - PIPE Combo v3.0
    - PIPE PCIe V3.0 R9

